package model;

import java.util.Map;

public  class Node {
    int value;
    int key;
    Node left;
    Node right;

    public Node(int value) {
        this.value = value;
    }

    public Node(int value, int key) {
        this.value = value;
        this.key = key;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
        //int oldValue= value; thi.value= value; return oldValue;
    }

    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                ", key=" + key +
                ", left=" + left +
                ", right=" + right +
                '}';
    }
}

