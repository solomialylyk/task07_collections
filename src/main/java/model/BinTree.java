package model;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class BinTree implements Map {
    Node root;
   static  int size;

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size==0 ? true : false;
    }

    public Node addRecursive(Node current, int key, int value) {
        if (current == null) {
            return new Node(key, value);
        }
        if (key < current.key) {
            current.left = addRecursive(current.left, key, value);
        } else if (key > current.key) {
            current.right = addRecursive(current.right,key,  value);
        } else {
            // value already exists
            return current;
        }

        return current;
    }
    public void add(int key, int value) {
        root= addRecursive(root, key, value);
        size++;
    }
    public BinTree createTree(){
        BinTree bin= new BinTree();
        bin.add(1,6);
        bin.add(3,7);
        bin.add(2,2);
        bin.add(5,3);
        return bin;

    }
    private Node deleteNode(Node current, int key) {
        if (current == null) {
            return null;
        }

        if (key == current.key) {
            // Node to delete found
            // ... code to delete the node will go here
            //a node has no children
            if (current.left == null && current.right == null) {
                return null;
            }
            //a node has children
            if (current.right == null) {
                return current.left;
            }
            if (current.left == null) {
                return current.right;
            }
            int smallestValue = findSmallestValue(current.right);
            current.value = smallestValue;
            current.right = deleteNode(current.right, smallestValue);
            return current;
        }
        if (key < current.key) {
            current.left = deleteNode(current.left, key);
            return current;
        }
        current.right = deleteNode(current.right, key);
        return current;
    }
    private int findSmallestValue(Node root) {
        return root.left == null ? root.value : findSmallestValue(root.left);
    }
    public void delete(int key) {
        root = deleteNode(root, key);
        size--;
    }

    public void print() {
        System.out.println(root);

    }

    public boolean containsKey(Object key) {
        return false;
    }

    public boolean containsValue(Object value) {
        return false;
    }

    public Object get(Object key) {
        return null;
    }

    public Object put(Object key, Object value) {
        return null;
    }

    public Object remove(Object key) {
        return null;
    }

    public void putAll(Map m) {

    }

    public void clear() {

    }

    public Set keySet() {
        return null;
    }

    public Collection values() {
        return null;
    }

    public Set<Entry> entrySet() {
        return null;
    }

    @Override
    public String toString() {
        return "BinTree{" +
                "root=" + root +
                ", size=" + size +
                '}';
    }
}
