import model.BinTree;

public class Application {
    public static void main(String[] args) {
        BinTree bt = new BinTree();
        bt.add(2,3);
        bt.add(1,4);
        bt.add(7,2);
        bt.add(5,6);

        System.out.println(bt);
        bt.delete(6);
        System.out.println(bt);
        bt.print();


    }
}
